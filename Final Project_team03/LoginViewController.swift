//
//  LoginViewController.swift
//  NWEvents
//
//  Created by Manoj Kumar Are on 4/9/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
    var store:KCSAppdataStore!
    let defaults = NSUserDefaults.standardUserDefaults()
    var userlogin:LoginUser!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
       // store = KCSAppdataStore.storeWithOptions([ // a store represents a local connection to the cloud data base
        //    KCSStoreKeyCollectionName : "OwnerInformation",
          //  KCSStoreKeyCollectionTemplateClass : Ownersinfo.self
          //  ])

    }
    
    @IBAction func loginAction(sender: AnyObject) {
        if (emailID.text!.isEmpty || password.text!.isEmpty)
        {
            self.displayAlertControllerWithTitle("Login Failed", message: "All fields are Required to fill")
        }
        else
        {
            userlogin = LoginUser(emailId: emailID.text!, password: password.text!)
           // Constants.userID = emailID.text
            loginuser(userlogin)
            //self.displayAlertControllerWithTitle("Login Successful", message: "")
        }

    }
    func loginuser(login:LoginUser) {
        
        KCSUser.loginWithUsername(
            login.emailId,
            password: login.password,
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil
                {
                    self.defaults.setValue(login.emailId,forKey: Constants.USERNAME)
                    self.defaults.synchronize()
                    self.displayAlertControllerWithSuccess("Login successful", message: "Welcome!")
                    //hide log-in view and show main app content
                }
                else
                {
                    //there was an error with the update save
                    let message = errorOrNil.localizedDescription
                    self.displayAlertControllerWithTitle("Login failed", message: message)
                }
            }
        )
    }
    
    func displayAlertControllerWithTitle(title:String, message:String)
    {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    
    func displayAlertControllerWithSuccess(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("loginsuccess", sender: self)}))
       self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
