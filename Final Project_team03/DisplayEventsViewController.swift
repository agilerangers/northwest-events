//
//  DisplayEventsViewController.swift
//  NWEvents
//
//  Created by Manoj Kumar Are on 4/9/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import UIKit

class DisplayEventsViewController: UIViewController {

    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var contact: UILabel!
    
    
    @IBOutlet weak var date: UIDatePicker!
    var eventtype:String!
    var eventname:String!
    var locations:String!
    var contacts:String!
    var date1:NSDate!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventType.text = eventtype
        eventName.text = eventname
        location.text = locations
        contact.text = contacts
        date.date = date1
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
