//
//  KinveyDBOperations.swift
//  Final Project_team03
//
//  Created by Are,Manoj Kumar on 4/3/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import Foundation

import UIKit

@objc protocol OperationProtocol {
    
   optional func onSuccess()
    
  optional  func onError(message:String)
    
  optional  func noActiveUser()
    
   optional func loginFailed()
    optional func fetchAll(event:[AnyObject])
    optional func myEventFetch(event:[AddEvent])
    
}



class KinveyDBOperations {
    
    
    
    let store:KCSAppdataStore!
    
    //let ownerPost:KCSAppdataStore!
    
    let operaionDelegate:OperationProtocol!
     let defaults = NSUserDefaults.standardUserDefaults()
    
    
    init(operationProtocol:OperationProtocol){
        
        self.operaionDelegate = operationProtocol
        
        
        
        store = KCSAppdataStore.storeWithOptions([
            
            KCSStoreKeyCollectionName : "AddEvent",
            
          KCSStoreKeyCollectionTemplateClass : AddEvent.self
            
            ])
        
       // ownerPost = KCSAppdataStore.storeWithOptions([
            
         //   KCSStoreKeyCollectionName : "OwnerPosts",
            
         //   KCSStoreKeyCollectionTemplateClass : OwnerPosts.self
            
          //  ])
        
        
        
    }
    
    
    
    
    
    func saveData() {
        
        if let _ = KCSUser.activeUser() {
            
            
            
        }else{
            
            operaionDelegate.noActiveUser!()
            
        }
        
    }
    
    
    
    // Mark: Save the owner Post
    
    
    
    func saveEvent(addEvent:AddEvent){
        
        store.saveObject(
            
            addEvent,
            
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil != nil {
                    
                    //save failed
                    
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                    
                } else {
                    
                    //save was successful
                    
                    print("Successfully saved event (id='%@').")
                    
                    self.operaionDelegate.onSuccess!()
                    
                    //, (objectsOrNil[0] as! NSObject).kinveyObjectId())
                    
                }
                
            },
            
            withProgressBlock: nil
            
        )
        
    }
    
    func FetchItem() {
        //listArray = ""
             //  print(query)
        store.queryWithQuery(
            KCSQuery(),
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil {
                    if let objects = objectsOrNil as [AnyObject]!{
                       // for object in objects{
                           // let event = object as! AddEvent
                            self.operaionDelegate.fetchAll!(objects)
                            
                         //  }
                    }
                    // print("****** \(self.listArray)")
                    
                } else {
                    
                    print("Error")
                    
                }
            },
            withProgressBlock: nil
        )
        
    }
    
    func fetchMyEvents(){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "emailId", withExactMatchForValue: userValue)
        
        store.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil {
                    
                    let objects = objectsOrNil as! [AddEvent]
                    
                    self.operaionDelegate.myEventFetch!(objects)
                    
                }
        
        
        
     else {
    
    print("Error")
    
    }
},
withProgressBlock: nil
)

}

   /*
    func registerUser(user:UserRegister){



        let userRows  = [

            UserRole:user.role.rawValue,
            
            KCSUserAttributeGivenname : user.firstName,
            
            KCSUserAttributeSurname : user.lastName,
            
            KCSUserAttributeEmail : user.emailID
            
        ]
        
        
        
        
        
        
        
        KCSUser.userWithUsername(
            
            user.emailID,
            
            password:user.password,
            
            fieldsAndValues: userRows,
            
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                
                if errorOrNil == nil {
                    
                    self.operaionDelegate.onSuccess(user)
                    
                } else {
                    
                    self.operaionDelegate.onError(errorOrNil.description)
                    
                }
                
            }
            
        )
        
        
        
        saveData()
        
        
        
        store.saveObject(
            
            user,
            
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil != nil {
                    
                    //save failed
                    
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                    
                    self.operaionDelegate.onError("something gone wrong!")
                    
                } else {
                    
                    //save was successful
                    
                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                    
                    //self.operaionDelegate.onSuccess()
                    
                }
                
            },
            
            withProgressBlock: nil
            
        )
        
        
        
    }
    
    func loginUser(userLogin:UserLogin){
        
        
        
        KCSUser.loginWithUsername(
            
            userLogin.email,
            
            password: userLogin.password,
            
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                
                if errorOrNil == nil {
                    
                    //self.displayAlertControllerWithTitle("Success!", message: "login successful")
                    
                    let message = "login successful"
                    
                    let alert = UIAlertView(
                        
                        title: NSLocalizedString("Create account successful", comment: "Sign account successful"),
                        
                        message: message,
                        
                        delegate: nil,
                        
                        cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
                        
                    )
                    
                    alert.show()
                    
                    self.emailConfirmation(user.email)
                    
                    self.operaionDelegate.onSuccess(user)
                    
                    
                    
                    
                    
                    
                    
                } else {
                    
                    //there was an error with the update save
                    
                    let message = errorOrNil.localizedDescription
                    
                    let alert = UIAlertView(
                        
                        title: NSLocalizedString("Create account failed", comment: "Sign account failed"),
                        
                        message: message,
                        
                        delegate: nil,
                        
                        cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
                        
                    )
                    
                    alert.show()
                    
                    self.operaionDelegate.onError("login failed!")
                    
                }
                
            }
            
        )
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    //func passwordReset(emailID:String) {
    
    //       KCSUser.sendPasswordResetForUser(emailID, withCompletionBlock: { (succeeded: Bool, error: NSError?) -> Void in
    
    //           if error == nil {
    
    //                if succeeded { // SUCCESSFULLY SENT TO EMAIL
    
    //                    print("Reset email sent to your inbox");
    
    //               }
    
    //                else { // SOME PROBLEM OCCURED
    
    //                }
    
    //            }
    
    //            else { //ERROR OCCURED, DISPLAY ERROR MESSAGE
    
    //                print(error!.description);
    
    //        }
    
    //        });
    
    
    
    
    
    //        KCSUser.sendPasswordResetForUser(
    
    //            emailID,
    
    //            withCompletionBlock: { (emailSent: Bool, errorOrNil: NSError!) -> Void in
    
    //            if errorOrNil == nil {
    
    //            
    
    //            print("Reset email sent to your inbox");
    
    //            }
    
    //            
    
    //            
    
    //            else { //ERROR OCCURED, DISPLAY ERROR MESSAGE
    
    //                self.operaionDelegate.onError("some problem has occurred")}
    
    //                }
    
    //        )
    
    //            emailID,
    
    //            withCompletionBlock: { (emailSent: Bool, errorOrNil: NSError!) -> Void in
    
    //                self.operaionDelegate.onError("Please enter valid emailID")
    
    //            }
    
    //)
    
    //}
    
    
    
    func emailConfirmation(email:String){
        
        print(email)
        
        
        
        let activeUser = KCSUser.activeUser()
        
        KCSUser.sendEmailConfirmationForUser(
            
            activeUser.email,
            
            withCompletionBlock: { (emailSent: Bool, errorOrNil: NSError!) -> Void in
                
                if errorOrNil != nil {
                    
                    self.operaionDelegate.onError("Please give valid emailID")
                    
                } // not much to do on success, for most apps
                
            }
            
        )
        
    }
    
    //    func displayAlertControllerWithTitle(title:String, message:String) {
    
    //        let uiAlertController:UIAlertController = UIAlertController(title: title,
    
    //            message: message, preferredStyle: UIAlertControllerStyle.Alert)
    
    //        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
    
    //            handler:{(action:UIAlertAction)->Void in }))
    
    //        self.presentViewController(uiAlertController, animated: true, completion: nil)
    
    //        
    
    //    }

*/
    
}