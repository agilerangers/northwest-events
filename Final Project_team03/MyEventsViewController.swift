//
//  MyEventsViewController.swift
//  NWEvents
//
//  Created by Kommula,Priyanka on 4/18/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import UIKit

class MyEventsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,OperationProtocol{
    
    var array:[AddEvent] = []
     var kinveyDBOperations:KinveyDBOperations!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        kinveyDBOperations = KinveyDBOperations(operationProtocol:self)
        kinveyDBOperations.fetchMyEvents()
        tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
       
     
            return array.count
        
    }
    //    override func viewWillAppear(animated: Bool) {
       // kinveyClientOperation.FetchItem()
    //    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelistdata", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
        let nameLbl : UILabel = cell.viewWithTag(12) as! UILabel
        nameLbl.text = array[indexPath.row].eventName
        
        
               return cell
    }
    override func viewWillAppear(animated: Bool) {
        kinveyDBOperations.fetchMyEvents()
        tableView.reloadData()

    }

    func myEventFetch(event: [AddEvent]) {
        array = event
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MyEventsDisplay"
        {
            if let destinationVC = segue.destinationViewController as? MyEventsDisplayViewController{
                if let displayIndex = tableView.indexPathForSelectedRow?.row {
                    destinationVC.eventtype = array[displayIndex].eventType
                    destinationVC.eventname = array[displayIndex].eventName
                    destinationVC.locations = array[displayIndex].location
                    destinationVC.contacts = array[displayIndex].contact
                    destinationVC.date1 = array[displayIndex].date.description
                    
                    
                    
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
