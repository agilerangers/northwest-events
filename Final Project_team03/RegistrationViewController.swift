//
//  RegistrationViewController.swift
//  NWEvents
//
//  Created by Manoj Kumar Are on 4/9/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var signup: UIButton!
     var store:KCSAppdataStore!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var registereduser:RegisterUser!
    
    
    func isValidEmail(testStr:String) -> Bool {
        
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    

    @IBAction func signupAction(sender: AnyObject) {
        if (emailID.text!.isEmpty || password.text!.isEmpty)
        {
            self.displayAlertControllerWithTitle("Registration Failed", message: "All fields are Required to fill")
        }
        else if  password.text != confirmPassword.text! {
            self.displayAlertControllerWithTitle("Mismatch", message: "password doesnot match")
        }
        else if (!isValidEmail(emailID.text!))
        {
            self.displayAlertControllerWithTitle("Invalid Email", message: "insert proper email address")
        }
        else
        {
            registereduser = RegisterUser(emailId: emailID.text!, password: password.text!)
            registerUser(registereduser)
        }
    }
    func registerUser(user:RegisterUser) {
        KCSUser.userWithUsername(
            user.emailId,
            password: user.password,
            
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil
                {
                    //the log-in was successful and the user is now the active user and credentials saved
                    self.displayAlertControllerWithTitle("registration successful", message: "Welcome!")
                    self.displayAlertControllerWithSuccess("registration successful", message: "Welcome!")
                    //hide log-in view and show main app content
                }
                else
                {
                    //there was an error with the update save
                    let message = errorOrNil.localizedDescription
                    self.displayAlertControllerWithTitle("registration failed", message: "try again")
                }
            }
            
        )
    }
    
    
    
    func displayAlertControllerWithSuccess(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("registrationSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
