//
//  AddEvent.swift
//  NWEvents
//
//  Created by Manoj Kumar Are on 4/9/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import Foundation

class AddEvent:NSObject{
    var eventType:String
    var eventName:String
    var location:String
    var date:NSDate
    var contact:String
    var entityId: String?
    var emailId:String
    var eventTime:String
    
    override init(){
        self.eventType = ""
        self.eventName = ""
        self.location = ""
        self.date = NSDate()
        self.eventTime = ""
        self.contact = ""
       self.emailId = ""
        
    }
    
    init(eventType:String,eventName:String,location:String,contact:String,date:NSDate,eventTime:String,emailId:String){
        self.eventType = eventType
        self.eventName = eventName
        self.location = location
       self.date = date
        self.eventTime = eventTime
        self.contact = contact
        self.emailId = emailId
        
    }
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "eventType" : "eventType",
            "eventName" : "eventName",
            "location" : "location",
            "contact":"contact",
            "date":"date",
            "eventTime":"eventTime",
            "emailId":"emailId"
            
            
        ]
    }
    
}