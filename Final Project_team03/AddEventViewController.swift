//
//  SecondViewController.swift
//  Final Project_team03
//
//  Created by Lakkadi,Madhurya on 3/28/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import UIKit

class AddEventViewController: UIViewController,UITextFieldDelegate,OperationProtocol {

    @IBOutlet weak var eventType: UITextField!
    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var date: UIDatePicker!
    
   
    @IBOutlet weak var eventTime: UITextField!
    @IBOutlet weak var contact: UITextField!
    @IBOutlet weak var postEvent: UIButton!
    var kinveyDBOpr:KinveyDBOperations!
      let defaults = NSUserDefaults.standardUserDefaults()
    
    
    var addEvent:AddEvent!

    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyDBOpr = KinveyDBOperations(operationProtocol:self)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func postEventAction(sender: AnyObject) {
       
      
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        addEvent = AddEvent(eventType: eventType.text!, eventName: eventName.text!, location: location.text!, contact: contact.text!,date: date.date,eventTime:eventTime.text!,emailId: userValue)
        kinveyDBOpr.saveEvent(addEvent)
        
        self.displayAlertControllerWithTitle("Success", message:"Saved")
        //self.navigationController!.popViewControllerAnimated(true)
    }
    
    func onSuccess(){
        self.displayAlertControllerWithSuccess("Event Posted Successfully", message:"")
    }
    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
        
    }
    func displayAlertControllerWithSuccess(title:String, message:String) {
        print("Event success")
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("EventSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
}

}