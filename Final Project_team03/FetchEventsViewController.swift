//
//  FetchEventsViewController.swift
//  NWEvents
//
//  Created by Manoj Kumar Are on 4/9/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import UIKit

class FetchEventsViewController: UIViewController,UIApplicationDelegate,UITableViewDataSource,OperationProtocol {
    var kinveyDBOperations:KinveyDBOperations!
    var array:[AddEvent] = []
    var todayEventsarray:[AddEvent] = []
    var upcomingEventsArray:[AddEvent] = []
    
    
    @IBOutlet weak var tableview: UITableView!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem=nil
        kinveyDBOperations = KinveyDBOperations(operationProtocol:self)
        kinveyDBOperations.FetchItem()
       
              //  print(upcomingEventsArray.count)
       // print(todayEventsarray.count)
        //print(array.count)
        
      
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
         self.tableview.reloadData()
    }
    
    
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
        
    }
    
    //UITableViewDelete
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var todaycnt:Int = 0
        var upcmgcnt:Int = 0
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        var components = calendar.components([.Day , .Month , .Year], fromDate: date)
        let year =  components.year
        let month = components.month
        let day = components.day
        print("\(NSDate())   ---")
        
        var rowyear = 0
        var rowmnth = 0
        var rowday = 0
        for(var i=0;i<array.count;i++){
            
            // print("in loop \(array[i].date)")
            components = calendar.components([.Day , .Month , .Year], fromDate: array[i].date)
            rowyear=components.year
            rowmnth=components.month
            rowday=components.day
            if(year == rowyear && month == rowmnth && day == rowday)
            {
                todaycnt++
            }
            else{
                upcmgcnt++
            }
        }
        
        if(section == 0){
            return todaycnt
        }
        else{
            return upcmgcnt
            
        }
        // return array.count
        
        
    }
    //    override func viewWillAppear(animated: Bool) {
    //        kinveyClientOperation.FetchItem()
    //    }
    
    func fetchAll(events : [AnyObject]) {
        
        for object in events{
            let event = object as! AddEvent
            array.append(event)
        }
        self.tableview.reloadData()
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "Today Events"
        }
        else {
            return "Upcoming Events"
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
        
        //    let eventNameLbl : UILabel = cell.viewWithTag() as! UILabel
        
        let eventNameLbl : UILabel = cell.viewWithTag(7) as! UILabel // function viewWithTag(Int) to update the labels in the table.
        
        // let location:UILabel = cell.viewWithTag(8) as! UILabel
        
        var todayEventsarr:[AddEvent] = []
        var upcomingEventsArr:[AddEvent] = []
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        var components = calendar.components([.Day , .Month , .Year], fromDate: date)
        let year =  components.year
        let month = components.month
        let day = components.day
        print("\(NSDate())   ---")
        
        var rowyear = 0
        var rowmnth = 0
        var rowday = 0
        for(var i=0;i<array.count;i++){
            
            // print("in loop \(array[i].date)")
            components = calendar.components([.Day , .Month , .Year], fromDate: array[i].date)
            rowyear=components.year
            rowmnth=components.month
            rowday=components.day
            if(year == rowyear && month == rowmnth && day == rowday)
            {
                todayEventsarr.append(array[i])
                
            }
            else{
                upcomingEventsArr.append(array[i])
            }
        }
        
        
        
        if(indexPath.section == 0){
            eventNameLbl.text = todayEventsarr[indexPath.row].eventName
        }
        else{
            eventNameLbl.text = upcomingEventsArr[indexPath.row].eventName
        }
        //eventTypeLbl.text = array[indexPath.row].eventType
        
        //  location.text = array[indexPath.row].location
        
        
        
        return cell
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Display"
        {
            
            
            var todayEventsarr:[AddEvent] = []
            var upcomingEventsArr:[AddEvent] = []
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            var components = calendar.components([.Day , .Month , .Year], fromDate: date)
            let year =  components.year
            let month = components.month
            let day = components.day
            // print("\(NSDate())   ---")
            
            var rowyear = 0
            var rowmnth = 0
            var rowday = 0
            for(var i=0;i<array.count;i++){
                
                // print("in loop \(array[i].date)")
                components = calendar.components([.Day , .Month , .Year], fromDate: array[i].date)
                rowyear=components.year
                rowmnth=components.month
                rowday=components.day
                if(year == rowyear && month == rowmnth && day == rowday)
                {
                    todayEventsarr.append(array[i])
                    
                }
                else{
                    upcomingEventsArr.append(array[i])
                }
            }
            
            print("num in today array \(todayEventsarr.count)")
            let sec = tableview.indexPathForSelectedRow?.section
            let displayIndex = tableview.indexPathForSelectedRow?.row
            
            print("sec no-- \(sec) row num  is \(displayIndex)")
            if let destinationVC = segue.destinationViewController as? DisplayEventsViewController{
                if sec == 0 {
                    destinationVC.eventtype = todayEventsarr[displayIndex!].eventType
                    destinationVC.eventname = todayEventsarr[displayIndex!].eventName
                    destinationVC.locations = todayEventsarr[displayIndex!].location
                    destinationVC.contacts = todayEventsarr[displayIndex!].contact
                    destinationVC.date1 = todayEventsarr[displayIndex!].date
                }
                else{
                    destinationVC.eventtype = upcomingEventsArr[displayIndex!].eventType
                    destinationVC.eventname = upcomingEventsArr[displayIndex!].eventName
                    destinationVC.locations = upcomingEventsArr[displayIndex!].location
                    destinationVC.contacts = upcomingEventsArr[displayIndex!].contact
                    destinationVC.date1 = upcomingEventsArr[displayIndex!].date
                }
                
                
                
            }
        }
    }
}

    

        