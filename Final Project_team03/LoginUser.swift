//
//  LoginUser.swift
//  NWEvents
//
//  Created by Manoj Kumar Are on 4/9/16.
//  Copyright © 2016 Lakkadi,Madhurya. All rights reserved.
//

import Foundation

class LoginUser: NSObject
{
    
    var emailId:String
    var password:String
    var entityId: String?
    
    init(emailId:String,password:String)
    {
        
        self.emailId = emailId
        self.password = password
    }
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "emailId" : "emailId",
            "password" : "password",
            
            
        ]
    }
    
}
